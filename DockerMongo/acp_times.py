"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
   """
   Args:
      control_dist_km:  number, the control distance in kilometers
      brevet_dist_km: number, the nominal distance of the brevet
         in kilometers, which must be one of 200, 300, 400, 600,
         or 1000 (the only official ACP brevet distances)
      brevet_start_time:  An ISO 8601 format date-time string indicating
         the official start time of the brevet
   Returns:
      An ISO 8601 format date string indicating the control open time.
      This will be in the same time zone as the brevet start time.


   """
   timeTotal = arrow.get(brevet_start_time)
   minuteShift = 0
   diff = 0
   BREVET_CUTOFFS = {(200, 0, 34), (400, 200, 32), (600, 400, 30), (1000, 600, 28)}   

   while(control_dist_km > 0):
      for cutoff in BREVET_CUTOFFS:
         upper_cutoff, lower_cutoff, speed = cutoff
         if (control_dist_km <= upper_cutoff and control_dist_km > lower_cutoff):
            diff = control_dist_km - lower_cutoff
            minuteShift += 60 * diff / speed
            control_dist_km -= diff

   return timeTotal.shift(minutes=round(minuteShift)).isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
   """
   Args:
      control_dist_km:  number, the control distance in kilometers
         brevet_dist_km: number, the nominal distance of the brevet
         in kilometers, which must be one of 200, 300, 400, 600, or 1000
         (the only official ACP brevet distances)
      brevet_start_time:  An ISO 8601 format date-time string indicating
         the official start time of the brevet
   Returns:
      An ISO 8601 format date string indicating the control close time.
      This will be in the same time zone as the brevet start time.
   """
   timeTotal = arrow.get(brevet_start_time)
   minuteShift = 0
   diff = 0
   if (control_dist_km == 0.0):
      return timeTotal.shift(hours=1).isoformat()
   elif (brevet_dist_km == 200 and control_dist_km == 200):
      return timeTotal.shift(hours=13.5).isoformat()
   else:
      BREVET_CUTOFFS = {(600, 0, 15), (1000, 600, 11.428)}
      while(control_dist_km > 0):
         for cutoff in BREVET_CUTOFFS:
            upper_cutoff, lower_cutoff, speed = cutoff
            if (control_dist_km <= upper_cutoff and control_dist_km > lower_cutoff):
               diff = control_dist_km - lower_cutoff
               minuteShift += 60 * diff / speed
               control_dist_km -= diff

   return timeTotal.shift(minutes=round(minuteShift)).isoformat()
