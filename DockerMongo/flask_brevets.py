"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import flask
from flask import Flask, redirect, url_for, request, render_template
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import os
from pymongo import MongoClient

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    brevet_dist = request.args.get('brevet_dist', type=float)
    sd = request.args.get('startDate')
    st = request.args.get('startTime')
    startTime = arrow.get(sd + " " + st, "YYYY-MM-DD HH:mm", tzinfo='US/Pacific').isoformat()
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    open_time = acp_times.open_time(km, brevet_dist, startTime)
    close_time = acp_times.close_time(km, brevet_dist, startTime)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

@app.route("/submit", methods=['POST'])
def submit():
    distance = request.form['distance']
    maxDist = float(distance) * 1.2
    miles = request.form.getlist('miles')
    km = request.form.getlist('km')
    location = request.form.getlist('location')
    opentimes = request.form.getlist('open')
    closetimes = request.form.getlist('close')
    controls = []
    errmsg = ""
    empty = True
    prevctrl = -1

    # For loop checks & control point formation
    for index, dummy in enumerate(miles):
        if (km[index] != ""):
            currctrl = float(km[index])
            empty = False
            if (currctrl > maxDist):
                errmsg = "Control point placed too far past maximum brevet distance."
            elif (km[index] == 'NaN'):
                errmsg = "Km cannot be NaN."
            elif (miles[index] != ""):
                if (currctrl < prevctrl):
                    errmsg = "Controls must be entered in ascending order based on km."
                prevctrl = currctrl
                controls.append("======= Control #" + str(index + 1) + " =======")
                controls.append("Miles: " +  str(miles[index]) + " Km: " + str(km[index]))
                if (location[index] != ""):
                    controls.append(" Location: " + location[index])
                controls.append("Open time: " + opentimes[index])
                controls.append("Closing time: " + closetimes[index])

    # Post for-loop checks
    if (empty):
        errmsg = "You must enter at least 1 control point for your brevet."

    if (errmsg == ""):
        brevet_doc = {
            'distance': distance,
            'begin_date': request.form['begin_date'],
            'begin_time': request.form['begin_time'],
            'controls': controls
        }
        db.tododb.insert_one(brevet_doc)
        return redirect(url_for('index'))

    else:
        item = {'errmsg': errmsg}
        return render_template('errorPage.html', item=item)

@app.route("/display", methods=['GET'])
def display():
    if (db.tododb.count() == 0):
        item = {'errmsg': "Database is empty. Nothing to display."}
        return render_template('errorPage.html', item=item)
    _brevets = db.tododb.find()
    brevets = [brevet for brevet in _brevets]
    return render_template('brevets.html', brevets=brevets)

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
