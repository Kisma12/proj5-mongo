# ACP Time Calculator
Brevets are a type of long-distance bicycle ride with several control points along the way. Each of these
control points has a time when it opens and closes, based on the distance of the control from the start
of the brevet and the minimum or maximum speed of the riders in the brevet.

This codebase allows you to calculate the open and close times for different control points along a brevet.
The code uses the RUSA guidelines for calculating the open and close times, as can be found here:
https://rusa.org/pages/acp-brevet-control-times-calculator

The maximum brevet distance is 1000 km, at least for all countries outside France, and so this codebase only
supports controls placed up to 1000 km from the start of the brevet.

## For Users
To use this codebase, build and run the Dockerfile included in the brevets directory. Once running, visit the
website and enter the desired start time, start date, control distances, and total brevet distance. After entering
each control's distance from the start of the brevet, the page will automatically update with the open and close
times for that control.

You can submit properly formed brevet data from the table into a database and fetch it back out again using the two
buttons labeled 'submit' and 'display'. Each brevet is numbered chronologically starting with the first.

## For Developers
The current implementation uses Flask, AJAX, and python to manage requests to/from the webpage.
MongoDB is used for the storage and retrieval of information. The appropriate errors are displayed on a custom webpage
when encountered if invalid data is attempting to enter the database.

## Author Information
* Author: Thomas Kismarton
* Contact email: tkismar2@uoregon.edu
* Function: Hosts a website that computes brevet control times & stores/displays formed brevets.